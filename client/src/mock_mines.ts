export var mock_mines = [ {
        "title" : "Elkview Mine",
        "text" : "The Elkview Coal Mine is a coal mine located in the British Columbia. The mine has coal reserves amounting to 220.6 million tonnes of coking coal, one of the largest coal reserves in Canada and the world. The mine has an annual production capacity of 4.19 million tonnes of coal.",
        "_links" : {
            "self" : {
                "href" : "http://localhost:8080/mines/56ab53d2bee82d9a6257f39a"
            }
        }
    }, {
        "title" : "Fording River Mine",
        "text" : "The Fording River Coal Mine is a coal mine located in the British Columbia. The mine has coal reserves amounting to 263.8 million tonnes of coking coal, one of the largest coal reserves in Canada and the world. The mine has an annual production capacity of 8.34 million tonnes of coal.",
        "_links" : {
            "self" : {
                "href" : "http://localhost:8080/mines/56ab53d2bee82d9a6257f39b"
            }
        }
    }, {
        "title" : "Raven Coal Mine Project",
        "text" : "The Raven coal mine is a proposed mining project in British Columbia undergoing a joint federal/provincial review. It involves Compliance Energy Corp (a small Canadian mining company) and partners Itochu Corporation (Japan) and LG International (Korea). It is also known as the “Comox Join Venture”. It involves 31 square kilometres underground on Vancouver Island between Beaufort Mountains and Baynes Sound and 2 km2 surface works 4.2 km southwest of Buckley Bay in upper Cowie Creek.",
        "_links" : {
            "self" : {
                "href" : "http://localhost:8080/mines/56ab53d2bee82d9a6257f39c"
            }
        }
    }, {
        "title" : "Stellarton Surface Coal Mine",
        "text" : "The mine began operations in 1980 and coal is extracted using truck and shovel mining. Coal mining has taken place in this area of Pictou County for more than 400 years, and the pit is the last operating coal mine in Nova Scotia Underground mining previously took place in the area where the Stellarton pit is located and occasionally remnants of the abandoned tunnels from underground mining can be seen on the pit walls. Once coal has been extracted, the surface is restored through reclamation.",
        "_links" : {
            "self" : {
                "href" : "http://localhost:8080/mines/56ab53d2bee82d9a6257f39d"
            }
        }
    } ];

