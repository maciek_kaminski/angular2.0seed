import {Component, View} from 'angular2/core';
import {FORM_DIRECTIVES, CORE_DIRECTIVES} from 'angular2/common';
import {RouteConfig, Router, Route, ROUTER_DIRECTIVES} from 'angular2/router';
import {RemoteService} from './remoteService';
import {HttpService} from "./fetch";
import {Mine} from "./mine";
import {HomeInfo} from "./homeInfo";

@Component({
    selector: 'app',
    templateUrl: `templates/template.html`,
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, ROUTER_DIRECTIVES]
})
@RouteConfig([
    new Route({path: '/', component: HomeInfo, name: 'Home'}),
    new Route({path: '/Mine/:id', component: Mine, name: 'Mine'})
])
export class App {
    list:Object[] = [];
    remoteService:RemoteService;
    httpService:HttpService;
    router: Router;

    constructor(private _remoteService:RemoteService, _httpService:HttpService, router: Router) {
        this.remoteService = _remoteService;
        this.httpService = _httpService;
        this.list = this.remoteService.getLinks();
        this.router = router;
    }

    vm: Object = {};
    searchByTitle(input, event){
        //var id = this.remoteService.getByTitle(input).then(status)
        //    .then(this.httpService.json)
        //    .then((response) => {
        //        var articles = response._embedded.articles;
        //        if(articles.length>0){
        //            var id = this.remoteService.extractDataFromMineURL(articles[0]._links.self.href);
        //            this.router.navigate(['./Article', {id: id}]);
        //        }
        //    });

        var id = this.remoteService.getByTitle(input)
            .then((response) => {
                if (response) {
                    var id = this.remoteService.extractDataFromMineURL(response._links.self.href);
                    this.router.navigate(['./Mine', {id: id}]);
                }
            });
    }
}
