import {Component, View} from 'angular2/core';
import { RouterLink, RouteParams } from 'angular2/router';
import {RemoteService} from './remoteService';
import {HttpService} from "./fetch";


@Component({
    selector: 'div[art]'
})
@View({
    templateUrl: `templates/mine.html`,
})
export class Mine {
    list: Object[] = [];
    title: string;
    text: string;
    constructor(remoteService:RemoteService, httpService:HttpService, routeParam: RouteParams) {
        var id:number;
        id = <number>routeParam.get("id");

        //remoteService.getArticle(id).then(status)
        //    .then(httpService.json)
        //    .then((response) => {
        //        this.title = response.title;
        //        this.text = response.text;
        //    })

        remoteService.getMine(id)
            .then((response) => {
                this.title = response.title;
                this.text = response.text;
            })
    }
}
