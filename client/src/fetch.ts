import {Injectable} from 'angular2/core';

@Injectable()
export class HttpService {

    status(response) {
        if (response.status >= 200 && response.status < 300) {
            return Promise.resolve(response);
        }
        return response.text().then(function (text) {
            throw new Error(text);
        });
    }

    text(response) {
        return response.text();
    }

    json(response) {
        return response.json();
    }

}