import {bootstrap} from 'angular2/platform/browser';
import {App} from './app';
import { HttpService } from './fetch'
import {RemoteService} from './remoteService';
import {ROUTER_PROVIDERS} from "angular2/router";
import {HashLocationStrategy} from "angular2/router";
import {LocationStrategy} from "angular2/router";
import {provide} from "angular2/core";


bootstrap(App, [RemoteService, HttpService, ROUTER_PROVIDERS, provide(LocationStrategy, {useClass: HashLocationStrategy})]);