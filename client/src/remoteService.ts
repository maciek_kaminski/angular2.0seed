import {Injectable} from 'angular2/core';
import { HttpService } from './fetch'
import {mock_mines} from './mock_mines';

@Injectable()
export class RemoteService {
    url: string;
    httpService:HttpService;

    constructor(_httpService:HttpService) {
        this.httpService = _httpService;
        this.url = 'http://localhost:8080/mines/';
    }

    getLinks() {
        var list: Object[] = [];

        //var all = window.fetch(this.url).then(status)
        //    .then(this.httpService.json)
        //    .then((response) => {
        //        var articles = response._embedded.articles;
        //        for (var v in articles)
        //        {
        //            list.unshift(this.newItem(articles[v].title, this.extractDataFromMineURL(articles[v]._links.self.href)))
        //        }
        //    });

        for (var v in mock_mines)
        {
            list.unshift(this.newItem(mock_mines[v].title, this.extractDataFromMineURL(mock_mines[v]._links.self.href)))
        }

        return list;
    }

    getByTitle(title:string) {
        //return window.fetch(this.url+'search/findByTitleLike?title='+title);
        for (var v in mock_mines)  {
            if(mock_mines[v].title.toLowerCase().indexOf(title.toLowerCase()) > -1){
                return Promise.resolve(mock_mines[v]);
            }
        }
        return Promise.resolve(null);
    }

    newItem(title:string, id:string) {
        return {
            'name': title,
            'id': id,
        };
    }


    getMine(nr:number){
        //return window.fetch(this.url + '/' + nr);

        for (var v in mock_mines)  {
            var id = this.extractDataFromMineURL(mock_mines[v]._links.self.href);
            if(id == nr ){
                return Promise.resolve(mock_mines[v]);
            }
        }
    }

    extractDataFromMineURL(url:string){
        return url.replace(this.url, "");
    }
}