# Angular2 with Gulp
  
  To run:
   `npm install`,
   `gulp`
   
   - Fully ready app will be located in [dist] directory.
     Now You can use any http server to serve it - f.ex. open [dist/index.html] from IntelliJ.
   
   - Run `gulp serve` to build the app, start the server and opne [index.html] in the browser and enable [WATCH] for files.  
    
   - Produced `js` files are being concatenated with their sourcemap, so You can easily use browsers debugger with [Typescript] source files.
        
   
