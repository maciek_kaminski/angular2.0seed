module.exports = function() {
    var client = './client/';
    var clientSrc = client + 'src/';
    var node_modules = './node_modules/';
    var dist = './dist/';
    var root = './';

    var config = {
        client: client,
        clientSrc: clientSrc,
        dist: './dist/',
        angular_node_libraries: [
            'angular2/bundles/angular2-polyfills.js',
            'systemjs/dist/system.src.js',
            'rxjs/bundles/Rx.js',
            'angular2/bundles/angular2.dev.js',
            'angular2/bundles/router.js'
        ],
        node_modules: node_modules,
        dist_lib: dist+'lib',
        dist_css: dist + 'css',
        dev_lib: client + 'lib',
        less: client + 'css/*.less',
        ts_config: root + 'tsconfig.json',
        index_file: client + 'index.html',
        ts_files: client + '**/*.ts',
        js_files: [client + '**/*.js' , '!' + client + 'lib/**'],
        template_files: client + 'templates/**/*.html',
        image_files: client + 'images/**/*.*',
        watch_files: [
            client + 'css/*.less',
            client + 'index.html',
            client + '**/*.ts',
            client + 'templates/**/*.html',
            client + 'images/**/*.*',
            client + '**/*.js'
        ]
    };

    return config;

};
