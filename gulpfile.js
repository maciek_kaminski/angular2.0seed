var gulp = require('gulp');
var config = require('./gulp.config')();
var path = require('path');
var sourcemaps = require('gulp-sourcemaps');
var ts = require('gulp-typescript');
var del = require('del');
var runSequence = require('run-sequence');
var tslint = require('gulp-tslint');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');

const browserSync = require('browser-sync');
const reload = browserSync.reload;


// ----- DEV -----------------------

gulp.task('copy:libs:dev', function(){
    var mappedPaths = config.angular_node_libraries.map(file => {return path.resolve(config.node_modules, file)});

    return gulp.src(mappedPaths, {base: config.node_modules})
        .pipe(gulp.dest(config.dev_lib));
});

//----------------------------------


gulp.task('clean', function(){
    return del(config.dist)
});

gulp.task('copy:assets', function(callback){
    runSequence('copy:js', 'copy:images', 'copy:templates', callback);
});

gulp.task('copy:js', function(){
    return gulp.src(config.js_files, { base : config.client })
        .pipe(gulp.dest(config.dist));
});

gulp.task('copy:templates', function(){
    return gulp.src(config.template_files, { base : config.client })
        .pipe(gulp.dest(config.dist));
});

gulp.task('copy:images', function(){
    return gulp.src(config.image_files, { base : config.client })
        .pipe(gulp.dest(config.dist));
});

gulp.task('build:styles', function() {
    return gulp
        .src(config.less)
        .pipe(plumber()) // exit gracefully if something fails after this
        .pipe(less())
        .pipe(autoprefixer({browsers: ['last 2 version', '> 5%']}))
        //.pipe(minifyCSS())
        .pipe(gulp.dest(config.dist_css));
});

gulp.task('build:index', function(){
    var mappedPaths = config.angular_node_libraries.map(file => {return path.resolve(config.node_modules, file)});

    var copyJsNPMDependencies = gulp.src(mappedPaths, {base: config.node_modules})
        .pipe(gulp.dest(config.dist_lib))

    var copyIndex = gulp.src(config.index_file)
        .pipe(gulp.dest(config.dist))
    return [copyJsNPMDependencies, copyIndex];
});

gulp.task('build:app', function(){
    var tsProject = ts.createProject(config.ts_config);
    var tsResult = gulp.src(config.ts_files)
		.pipe(sourcemaps.init())
        .pipe(ts(tsProject))
	return tsResult.js
        .pipe(sourcemaps.write())
		.pipe(gulp.dest(config.dist))
});


// ----------------------------------

gulp.task('tslint', function() {
    return gulp.src(config.ts_config)
        .pipe(tslint())
        .pipe(tslint.report('verbose'));
});

// Run browsersync for development
gulp.task('serve', ['build'], function() {
    browserSync({
        server: {
            baseDir: config.dist
        }
    });

    gulp.watch(config.watch_files, ['buildAndReload']);
});

//----------------------------------------------

gulp.task('build', function(callback){
    runSequence('clean', 'build:index', 'copy:assets', 'build:styles', 'build:app', callback);
});

gulp.task('default', ['build']);
gulp.task('buildAndReload', ['build'], reload);